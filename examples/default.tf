module "eip_instance1" {
  source            = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-eip.git?ref=master"
  name              = "instance1"
  vpc               = true
  network_interface = module.ec2_instance_instance1.network_interface_id
}
