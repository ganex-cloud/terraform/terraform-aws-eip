resource "aws_eip" "this" {
  associate_with_private_ip = var.associate_with_private_ip
  instance                  = var.instance
  network_interface         = var.network_interface
  public_ipv4_pool          = var.public_ipv4_pool
  tags                      = merge(var.tags, { "Name" = var.name })
  vpc                       = var.vpc
}
