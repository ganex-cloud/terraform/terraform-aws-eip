variable "name" {
  description = "Name of the EIP resource"
}

variable "vpc" {
  description = "(Optional) Boolean if the EIP is in a VPC or not."
  type        = bool
  default     = null
}

variable "instance" {
  description = "(Optional) EC2 instance ID."
  type        = string
  default     = null
}

variable "network_interface" {
  description = "(Optional) Network interface ID to associate with."
  type        = string
  default     = null
}

variable "associate_with_private_ip" {
  description = "(Optional) User-specified primary or secondary private IP address to associate with the Elastic IP address. If no private IP address is specified, the Elastic IP address is associated with the primary private IP address."
  type        = string
  default     = null
}

variable "tags" {
  description = "(Optional) Map of tags to assign to the resource. Tags can only be applied to EIPs in a VPC."
  type        = map(string)
  default     = {}
}

variable "public_ipv4_pool" {
  description = "(Optional) EC2 IPv4 address pool identifier or amazon. This option is only available for VPC EIPs."
  type        = string
  default     = null
}
